<?php

namespace Thortech\Fortress;

use Thortech\Fortress\Actions\Fortify as Actions;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Fortify::createUsersUsing(Actions\CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(Actions\UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(Actions\UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(Actions\ResetUserPassword::class);

        RateLimiter::for('login', function (Request $request) {
            return Limit::perMinute(5)->by($request->email.$request->ip());
        });

        RateLimiter::for('two-factor', function (Request $request) {
            return Limit::perMinute(5)->by($request->session()->get('login.id'));
        });
    }
}
