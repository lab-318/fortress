<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FortressAlterUsersTable extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('users', 'ref')) {
            Schema::table('users', function (Blueprint $table) {
                $table->uuid('ref')
                    ->after('id')
                    ->nullable()
                    ->index();
            });
        }

        if (!Schema::hasColumns('users', ['two_factor_secret', 'two_factor_recovery_codes'])) {
            Schema::table('users', function (Blueprint $table) {
                $table->text('two_factor_secret')
                    ->after('password')
                    ->nullable();
                $table->text('two_factor_recovery_codes')
                    ->after('two_factor_secret')
                    ->nullable();
            });
        }

        if (!Schema::hasColumn('users', 'deleted_at')) {
            Schema::table('users', function (Blueprint $table) {
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(
                'ref',
                'two_factor_secret',
                'two_factor_recovery_codes',
                'deleted_at'
            );
        });
    }
}
